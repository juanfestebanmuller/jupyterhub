FROM jupyterhub/jupyterhub:5.2.1

LABEL MAINTAINER "benjamin.bertrand@esss.se"

# Install docker in the container
RUN curl https://get.docker.com -s --output /tmp/getdocker \
  && chmod +x /tmp/getdocker \
  && sh /tmp/getdocker \
  && rm -f /tmp/getdocker

# Install dockerspawner and ldapauthenticator
# Use a specific commit for ldapauthenticator (patch needed)
RUN /usr/local/bin/pip install --no-cache-dir \
    dockerspawner==13.0.0 \
    jupyterhub-ldapauthenticator==2.0.0
