# Configuration file for JupyterHub
import os
from tornado import gen
from jupyterhub.auth import DummyAuthenticator

username = "dummy"

class DummyAuthenticatorAuthState(DummyAuthenticator):
    @gen.coroutine
    def pre_spawn_start(self, user, spawner):
        # setup environment
        spawner.environment["NB_UID"] = "3600"
        spawner.environment["NB_GID"] = "3600"
        spawner.environment["NB_USER"] = username
        spawner.environment["NB_GROUP"] = "operators"
        spawner.environment["NB_HOME"] = '/home/'+username

c.JupyterHub.authenticator_class = DummyAuthenticatorAuthState
c.Authenticator.whitelist = {"operator", "dummy"}
c.DummyAuthenticator.password = "password"

c.Spawner.default_url = "/lab"
# Spawn single-user servers as Docker containers
c.JupyterHub.spawner_class = "docker"
# Spawn containers from this image
c.DockerSpawner.image = "registry.esss.lu.se/ics-docker/notebook"
# JupyterHub requires a single-user instance of the Notebook server, so we
# default to using the `start-singleuser.sh` script included in the
# jupyter/docker-stacks *-notebook images as the Docker run command when
# spawning containers.
c.DockerSpawner.extra_create_kwargs = {'user' : 'root', 'command': 'start-singleuser.sh'}
# Connect containers to this Docker network
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.network_name = "jupyterhub-network"
# Pass the network name as argument to spawned containers
c.DockerSpawner.extra_host_config = {"network_mode": "jupyterhub-network"}
# Explicitly set notebook directory because we'll be mounting a host volume to
# it.  Most jupyter/docker-stacks *-notebook images run the Notebook server as
# user `jovyan`, and set the notebook directory to `/home/jovyan/work`.
# We follow the same convention.
notebook_dir = f"~/notebooks"
c.DockerSpawner.notebook_dir = notebook_dir
# Remove containers once they are stopped
c.DockerSpawner.remove_containers = True
# For debugging arguments passed to spawned containers
c.DockerSpawner.debug = True

c.DockerSpawner.volumes = { os.environ['HOST_WORKING_DIRECTORY']: "/shares" }

# User containers will access hub by container name on the Docker network
c.JupyterHub.hub_ip = "jupyterhub"
c.JupyterHub.hub_port = 8080

# Port used by reverse proxy to access Jupyterhub
c.JupyterHub.port = 8000

# Persist hub data on volume mounted inside container
# c.JupyterHub.db_url = 'sqlite:////data/jupyterhub.sqlite'
# c.JupyterHub.cookie_secret_file = '/data/jupyterhub_cookie_secret'

c.Authenticator.admin_users = {"juanfestebanmuller"}
c.JupyterHub.admin_access = False
